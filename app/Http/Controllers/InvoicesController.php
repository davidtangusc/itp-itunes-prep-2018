<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class InvoicesController extends Controller
{
  public function index()
  {
    $invoices = DB::table('invoices')
      ->join('customers', 'invoices.CustomerId', '=', 'customers.CustomerId')
      ->orderBy('InvoiceDate', 'desc')
      ->limit(10)
      ->get();

    return view('invoices', [
      'invoices' => $invoices
    ]);
  }

  public function show($id)
  {
    $invoiceItems = DB::table('invoice_items')
      ->select('Quantity', 'invoice_items.UnitPrice as UnitPrice', 'artists.Name as artistName', 'tracks.Name as trackName')
      ->join('tracks', 'invoice_items.TrackId', '=', 'tracks.TrackId')
      ->join('albums', 'tracks.AlbumId', '=', 'albums.AlbumId')
      ->join('artists', 'artists.ArtistId', '=', 'albums.ArtistId')
      ->where('InvoiceId', '=', $id)
      ->get();

    return view('invoice-details', [
      'invoiceItems' => $invoiceItems
    ]);
  }
}
